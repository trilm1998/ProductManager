package com.keysoft.view;

import com.keysoft.controller.ProductController;
import com.keysoft.model.ProductModel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class GDAddSP extends JFrame {

    private List<ProductModel> products = new ArrayList<>();
    private JPanel contentPane;
    private JTextField txtid;
    private JTextField txtname;
    private JTextField txtdescription;
    private JTextField txtprice;
    private JTextField txtimportprice;
    private JTextField txttotal;
    private JTextField txtdate;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    GDAddSP frame = new GDAddSP();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public GDAddSP() {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 1072, 520);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel id = new JLabel("ID:");
        id.setHorizontalAlignment(SwingConstants.LEFT);
        id.setBounds(50, 100, 74, 26);
        contentPane.add(id);

        JLabel name = new JLabel("Name:");
        name.setHorizontalAlignment(SwingConstants.LEFT);
        name.setBounds(50, 136, 74, 26);
        contentPane.add(name);

        JLabel price = new JLabel("Price:");
        price.setHorizontalAlignment(SwingConstants.LEFT);
        price.setBounds(50, 208, 68, 26);
        contentPane.add(price);

        JLabel description = new JLabel("Description:");
        description.setHorizontalAlignment(SwingConstants.LEFT);
        description.setBounds(50, 172, 68, 26);
        contentPane.add(description);

        JLabel importPrice = new JLabel("ImportPrice:");
        importPrice.setHorizontalAlignment(SwingConstants.LEFT);
        importPrice.setBounds(50, 244, 68, 26);
        contentPane.add(importPrice);

        JLabel total = new JLabel("Total:");
        total.setHorizontalAlignment(SwingConstants.LEFT);
        total.setBounds(50, 280, 68, 27);
        contentPane.add(total);

        JLabel date = new JLabel("Date:");
        date.setHorizontalAlignment(SwingConstants.LEFT);
        date.setBounds(50, 317, 74, 26);
        contentPane.add(date);

        JLabel lblNewLabel = new JLabel("INSERT PRODUCT");
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setBounds(354, 24, 177, 35);
        contentPane.add(lblNewLabel);

        JButton insert = new JButton("Insert");
        insert.addActionListener(new ActionListener() {
            // action insert
            public void actionPerformed(ActionEvent e) {

                ProductModel pm = new ProductModel();
                pm.setId(Integer.parseInt(txtid.getText()));
                pm.setName(txtname.getText());
                pm.setDescription(txtdescription.getText());
                pm.setPrice(Integer.parseInt(txtprice.getText()));
                pm.setImportPrice(Integer.parseInt(txtimportprice.getText()));
                pm.setTotal(Integer.parseInt(txttotal.getText()));
                pm.setImportDate(txtdate.getText());

                ProductController prod = new ProductController();
                prod.insertProduct(pm);
            }
        });
        insert.setBounds(50, 390, 85, 21);
        contentPane.add(insert);

        JButton reset = new JButton("Reset");
        reset.addActionListener(new ActionListener() {
            // action reset
            public void actionPerformed(ActionEvent e) {
                txtname.setText("");
                txtdescription.setText("");
                txtprice.setText(null);
                txtimportprice.setText("");
                txttotal.setText(null);
                txtdate.setText("");

            }
        });
        reset.setBounds(171, 390, 85, 21);
        contentPane.add(reset);

        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(new ActionListener() {
            // action cancel
            public void actionPerformed(ActionEvent e) {
                new GDQLSP().setVisible(true);
            }
        });
        cancel.setBounds(300, 390, 85, 21);
        contentPane.add(cancel);

        txtid = new JTextField();
        txtid.setBounds(134, 101, 248, 26);
        contentPane.add(txtid);
        txtid.setColumns(10);

        txtname = new JTextField();
        txtname.setColumns(10);
        txtname.setBounds(134, 137, 248, 26);
        contentPane.add(txtname);

        txtdescription = new JTextField();
        txtdescription.setColumns(10);
        txtdescription.setBounds(135, 173, 248, 26);
        contentPane.add(txtdescription);

        txtprice = new JTextField();
        txtprice.setColumns(10);
        txtprice.setBounds(135, 209, 248, 26);
        contentPane.add(txtprice);

        txtimportprice = new JTextField();
        txtimportprice.setColumns(10);
        txtimportprice.setBounds(135, 245, 248, 26);
        contentPane.add(txtimportprice);

        txttotal = new JTextField();
        txttotal.setColumns(10);
        txttotal.setBounds(135, 281, 248, 26);
        contentPane.add(txttotal);

        txtdate = new JTextField();
        txtdate.setColumns(10);
        txtdate.setBounds(134, 315, 249, 26);
        contentPane.add(txtdate);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setBounds(400, 100, 666, 311);
        contentPane.add(tabbedPane);
    }

}
