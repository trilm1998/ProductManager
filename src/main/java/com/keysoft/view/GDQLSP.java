package com.keysoft.view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GDQLSP extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GDQLSP frame = new GDQLSP();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GDQLSP() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel productmanager = new JLabel("Product Manager");
		productmanager.setFont(new Font("Tahoma", Font.BOLD, 13));
		productmanager.setHorizontalAlignment(SwingConstants.CENTER);
		productmanager.setBounds(127, 10, 195, 37);
		contentPane.add(productmanager);

		JButton insertProduct = new JButton("InsertProduct");
		insertProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new GDAddSP().setVisible(true);

			}
		});
		insertProduct.setBounds(147, 70, 145, 21);
		contentPane.add(insertProduct);

		JButton editProduct = new JButton("EditProduct");
		editProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		editProduct.setBounds(147, 117, 145, 21);
		contentPane.add(editProduct);

		JButton deleteProduct = new JButton("DeleteProduct");
		deleteProduct.setBounds(147, 160, 145, 21);
		contentPane.add(deleteProduct);

		JButton exit = new JButton("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exit.setBounds(147, 194, 145, 21);
		contentPane.add(exit);
	}
}
