package com.keysoft.model;

import lombok.Data;

import java.util.Date;

@Data
public class ProductModel {

    private Integer id;

    private String name;

    private String description;

    private Integer price;

    private Integer importPrice;

    private Integer total;

    private String importDate;

    private String image;

    private Date createDate;

}