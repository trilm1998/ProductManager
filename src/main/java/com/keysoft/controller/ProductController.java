package com.keysoft.controller;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.keysoft.model.ProductModel;

public class ProductController {

	static final String DB_URL = "jdbc:mysql://localhost:3306/javaswing";
	static final String USER = "root";
	static final String PASS = "0986421693";
	static final String QUERY = "SELECT id, name_product, description_product, price, import_price, total, import_date FROM product";

	public void insertProduct(ProductModel prod) {

		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {

			System.out.println("Inserting records into the table...");
			String sql = "INSERT INTO product(name_product, description_product, price, import_price, total, create_date)"
					+ "VALUES(?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, prod.getName());
			ps.setString(2, prod.getDescription());
			ps.setInt(3, prod.getPrice());
			ps.setInt(4, prod.getImportPrice());
			ps.setInt(5, prod.getTotal());
			ps.setDate(6, new Date(Calendar.getInstance().getTimeInMillis()));

			int rs = ps.executeUpdate();

			if (rs == 1) {
				System.out.println("Insert success");
			}
		} catch (SQLException e) {
			System.out.println("Insert error");
			e.printStackTrace();
		}

	}

	public void editProduct(ProductModel prod) {

	}

	public void deleteProduct() {

	}

	public List<ProductModel> getAll() {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {
			String sql = "SELECT id, name_product, description_product, price, import_price, total, create_date FROM product";
			Statement statement = conn.createStatement();
			ResultSet rs = statement.executeQuery(sql);

			List<ProductModel> result = new ArrayList<>();
			if (rs != null) {
				while (rs.next()) {
					ProductModel model = new ProductModel();
					model.setId(rs.getInt("id"));
					model.setName(rs.getString("name_product"));
					model.setDescription(rs.getString("description_product"));
					model.setPrice(rs.getInt("price"));
					model.setImportPrice(rs.getInt("import_price"));
					model.setTotal(rs.getInt("total"));
					model.setCreateDate(rs.getDate("create_date"));

					result.add(model);
				}
			}


			return result;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
